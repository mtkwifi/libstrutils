#
# All files should be copied without any modification. If you found
# a bug or if you want something to be supported. Please file
# bug against this template/project_makefile.git project.
#

export STAGING_DIR=/Volumes/openwrt/RainCloud/barrier_breaker/staging_dir/

PREFIX=/usr
LIBDIR=$(PREFIX)/lib
INCLUDEDIR=$(PREFIX)/include
CROSS_COMPILE=/Volumes/openwrt/RainCloud/barrier_breaker/staging_dir/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2/bin/mipsel-openwrt-linux-

CC=$(CROSS_COMPILE)gcc
CXX=$(CROSS_COMPILE)g++
AR=$(CROSS_COMPILE)ar
LD=$(CROSS_COMPILE)ld
STRIP=$(CROSS_COMPILE)strip
AWK=awk

ifdef V
  ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
  endif
endif
ifndef KBUILD_VERBOSE
  KBUILD_VERBOSE = 0
endif

ifeq ($(KBUILD_VERBOSE),1)
  quiet =
  Q =
else
  quiet=quiet_
  Q = @
endif

MAKEFLAGS += -rR --no-print-directory
PHONY := __all

ifeq ($(KBUILD_SRC),)

ifeq ("$(origin O)", "command line")
  KBUILD_OUTPUT := $(O)
endif

ifneq ($(KBUILD_OUTPUT),)
saved-output := $(KBUILD_OUTPUT)
KBUILD_OUTPUT := $(shell cd $(KBUILD_OUTPUT) && /bin/pwd)
$(if $(KBUILD_OUTPUT),, \
	$(error output directory "$(saved-output)" does not exist))

$(MAKECMDGOALS) __all: sub-make
	@:

PHONY += $(MAKECMDGOALS) sub-make
sub-make: FORCE
	$(Q)$(MAKE) -C $(KBUILD_OUTPUT) \
		KBUILD_SRC=$(CURDIR) -f $(CURDIR)/Makefile \
		$(MAKECMDGOALS)

skip-makefile := 1
endif # ifneq ($(KBUILD_OUTPUT),)
endif # ifeq ($(KBUILD_SRC),)

ifeq ($(skip-makefile),)

__all: all

srctree := $(if $(KBUILD_SRC),$(KBUILD_SRC),$(CURDIR))
objtree := $(CURDIR)
src := $(srctree)
obj := .

VPATH := $(srctree)

CFLAGS :=-Wall -MMD
CXXFLAGS :=-Wall -MMD

CFLAGS += -include config.h

config-targets := 0
ifneq ($(filter %defconfig, $(MAKECMDGOALS)),)
	config-targets := 1
endif

ifeq ($(config-targets),1)
#======================
# *defconfig targets
%_defconfig: $(srctree)/conf2h.awk
	cp $(srctree)/configs/$@ $(obj)/.config
else
#======================
# build targets

obj-y :=
bin-y :=
lib-y :=
liba-y :=
libso-y :=
libheaders-y :=
install-extra-y :=
install-symlink-y :=

-include auto.conf
include $(srctree)/Kbuild
include $(srctree)/Makefile.include

#
# these template targets will link all the root/*.o and subdir/built-in.o
# Note there is no root/built-in.o being built from Makefile.build
#
define bin_tmpl
$(1): $$(patsubst %/,%/built-in.o,$$($(1)-obj-y)) $$(libdep)
	$$(call cmd,bin_o_target)
endef

define liba_tmpl
$(1).a: $$(patsubst %/,%/built-in.o,$$($(1)-obj-y))
	$$(call cmd,lib_a_target)
endef

define libso_tmpl
$(1).so: $$(patsubst %/,%/built-in.o,$$($(1)-obj-y))
	$$(call cmd,lib_so_target)
endef

define symlink_tmpl
$(1): $(2)
	rm -f $(DESTDIR)$(PREFIX)/$$(patsubst _install_%,%,$$@) ;
	ln -s $$(patsubst _install_%,%,$$<) $(DESTDIR)$(PREFIX)/$$(patsubst _install_%,%,$$@)
endef

liba-y += $(lib-y)
libso-y += $(lib-y)
# never touch lib-y again

$(foreach bin,$(bin-y),$(eval $(call bin_tmpl,$(bin))))
$(foreach lib,$(liba-y),$(eval $(call liba_tmpl,$(lib))))
$(foreach lib,$(libso-y),$(eval $(call libso_tmpl,$(lib))))

# create all the subdir/built-in.o dependency
obj-y := $(foreach bin,$(bin-y),$($(bin)-obj-y))
obj-y += $(foreach lib,$(liba-y),$($(lib)-obj-y))
obj-y += $(foreach lib,$(libso-y),$($(lib)-obj-y))
dep-y := $(patsubst %.o,%.d,$(filter-out %/,$(obj-y)))

subdir-y := $(patsubst %/,%,$(filter %/,$(obj-y)))
subdir-obj-y := $(patsubst %,%/built-in.o,$(subdir-y))

$(sort $(subdir-obj-y)): $(subdir-y) ;

PHONY += $(subdir-y)
$(subdir-y):
	$(Q)$(MAKE) $(build)=$@

ifneq ($(install-extra-y),)
install-subdir-y := $(patsubst %/,%,$(filter %/,$(install-extra-y)))
install-extra-y := $(filter-out %/,$(install-extra-y))
ifneq ($(strip $(install-subdir-y)),)
PHONY += $(install-subdir-y)
$(install-subdir-y):
	$(Q)$(MAKE) $(build)=$@ install
endif
endif

# create the lib*.so and lib*.a target when libso-y, liba-y is specified
ifneq ($(strip $(liba-y) $(libso-y)),)
libso-y := $(patsubst %,%.so,$(libso-y))
liba-y := $(patsubst %,%.a,$(liba-y))
endif

all: config.h $(liba-y) $(libso-y) $(bin-y)

auto.conf: .config
	if ! cmp -s $< $@ ; then cp -f $< $@ ; fi

config.h: auto.conf
	$(AWK) -f $(srctree)/conf2h.awk $< > $@

#
# Other targets: clean, install
#
clean:
	rm -f $(bin-y) $(liba-y) $(libso-y)
	@find . \
		\( -name '*.[oa]' \
		-o -name '*.d' \) \
		-type f -print | xargs rm -f

inst_symlink := $(foreach pair,$(install-symlink-y), \
	_install_$(word 1,$(subst ->, ,$(pair))))
inst_extras := $(addprefix _install_,$(install-extra-y))
inst_progs := $(addprefix _install_,$(bin-y))
inst_libso := $(addprefix _install_,$(libso-y))
inst_liba := $(addprefix _install_,$(liba-y))
inst_headers := $(addprefix _install_,$(libheaders-y))
PHONY += $(inst_progs) $(inst_libso) $(inst_liba) $(inst_headers) $(inst_extras)
PHONY += $(inst_symlink)

$(foreach pair,$(install-symlink-y), \
	$(eval $(call symlink_tmpl,$(addprefix _install_,$(firstword $(subst ->, ,$(pair)))),$(addprefix _install_,$(lastword $(subst ->, ,$(pair)))))))

$(inst_progs):
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $(patsubst _install_%,%,$@) $(DESTDIR)$(PREFIX)/bin
	$(STRIP) $(DESTDIR)$(PREFIX)/bin/$(patsubst _install_%,%,$@)

$(inst_liba) $(inst_libso):
	mkdir -p $(DESTDIR)$(PREFIX)/lib
	cp $(patsubst _install_%,%,$@) $(DESTDIR)$(LIBDIR)

$(inst_headers):
	mkdir -p $(DESTDIR)$(PREFIX)/include/$(dir $(patsubst _install_%,%,$@))
	cp $(srctree)/$(notdir $(patsubst _install_%,%,$@)) $(DESTDIR)$(INCLUDEDIR)/$(patsubst _install_%,%,$@)

$(inst_extras):
	mkdir -p $(DESTDIR)$(PREFIX)/$(dir $(patsubst _install_%,%,$@))
	cp $(srctree)/$(notdir $(patsubst _install_%,%,$@)) $(DESTDIR)$(PREFIX)/$(patsubst _install_%,%,$@)

install: $(inst_progs) $(inst_libso) $(inst_extras) $(install-subdir-y) $(inst_symlink)
install-dev: $(inst_headers) $(inst_liba) $(inst_libso)
install-symlink: $(inst_symlink)

project_configs := $(wildcard $(srctree)/configs/*_defconfig)
project_configs := $(notdir $(project_configs))

PHONY += help
help:
	@echo 'Targets supported:'
	@echo ''
	@echo 'all             - build all binaries'
	@echo 'install         - install files to $$(DESTDIR)'
	@echo 'install-dev     - install files defined by libheaders to $$(DESTDIR)'
	@echo 'install-symlink - install files defined by install-symlink'
	@echo 'clean           - delete all generated files'
	@echo 'help            - print this help'
	@echo ''
	@echo 'Config targets:'
	@$(if $(project_configs), \
		$(foreach c,$(project_configs), \
		printf " %-24s - Build for %s\\n" $(c) $(subst _defconfig,,$(c));) \
		echo '')

-include $(dep-y)

endif #config-targets

endif #skip-makefile

.PHONY: __all all clean install $(PHONY) FORCE

# these variables are needed by subdir descendent submake
export srctree objtree CFLAGS CXXFLAGS CC CXX AR LD Q quiet KBUILD_SRC VPATH DESTDIR PREFIX
