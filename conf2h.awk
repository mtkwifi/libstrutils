BEGIN {
	FS="="
	print "/* Automatically generated C config: don't edit */"
	print ""
}

# skip comment and blank lines
!/^($|[[:space:]]*#)/ {
	key = $1
	value = 0

	# remove inline comments
	sub(/#.*/, "", $2)

	if ($2 ~ /^y\s*/) {
		value = 1
	} else {
		value = $2
	}

	print "#define", key, value
}
