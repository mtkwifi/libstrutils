#include "strutils_taglist.h"

/* Free the memory of the single tag item */
static void free_tagitem(tagItemType *item)
{
	if (item) {
		if (item->tag)
			free(item->tag);
		if (item->value)
			free(item->value);
	}
	return;
}

/* Clear tag list */
static void empty_taglist(tagListType *list)
{
	if (list) {
		tagItemType **items = list->item;
		while(list->size) {
			free_tagitem(items[--(list->size)]);
		}
	}
	return;
}

/* Create a tag list */
static tagListType *create_taglist(void)
{
	tagListType *taglist;
	const int default_memsize = BUF_SIZE;

	taglist = (tagListType *)calloc(1, sizeof(tagListType));
	if (taglist) {
		/* Allocate numbers of tag items for storing tag and value */
		taglist->item = (tagItemType **)malloc(sizeof(tagItemType *) * default_memsize);
		taglist->memsize = default_memsize;
	}

	return taglist;
}

/* Create a single tag item: Tag="Value" */
static tagItemType *create_tagitem(char *tag, char *value)
{
	tagItemType *item = NULL;

	if (tag && value) {
		item = (tagItemType *)malloc(sizeof(tagItemType));
		item->tag = strdup(tag);
		item->value = strdup(value);
	}

	return item;
}

/* Search a single tag item in tag list */
static int search_taglist(tagListType *list, char *tag)
{
	int i;
	int pos = -1;

	/* Loop all list to find out if tag exists */
	for (i = 0; i < list->size; i++) {
		if (strcmp(list->item[i]->tag, tag) == 0) {
			pos = i;
			break;
		}
	}

	return pos;
}

/* Check if the specific tag in the tag list */
static int exist_taglist(tagListType *list, char *tag)
{
	return search_taglist(list, tag) >= 0 ? 1 : 0;
}

/* Get Tag and Value from string line */
static tagItemType *parse_tagitem(char *str)
{
	char *s = str;
	char *p = NULL;
	tagItemType *result = NULL;

	if (s && *s && *s != '#') {
		int e = strlen(s) - 1;
		while (e >= 0 && (s[e] == '\r' || s[e] == '\n')) {
			s[e--] = 0;
		}

		if (e >= 0 && s[e] == '"') {
			s[e] = 0;
		}
		p = strchr(s, '=');
	}

	if (p && *p == '=') {
		*p = 0;
		p++;
		if (*p == '"')
			p++;

		result = create_tagitem(s, p);
	}
	return result;
}

/* Insert a tag item in tag list */
static int insert_taglist(tagListType *list, tagItemType *tagItem)
{
	int pos;

	/* Check if we already has the tag */
	if (exist_taglist(list, tagItem->tag)) {
		free_tagitem(tagItem);
		return 0;
	}
	pos = list->size;

	/* Re-allocate more memory if it's not enough */
	if (list->size >= list->memsize) {
		int increment_size = BUF_SIZE;
		list->item = (tagItemType **)realloc(list->item, sizeof(tagItemType *) * (list->memsize + increment_size));
		list->memsize += increment_size;
	}
	if (pos < list->size) {
		memmove(list->item + (pos + 1), list->item + pos, sizeof(tagItemType *)*(list->size - pos));
	}

	list->item[pos] = tagItem;
	list->size++;

	/* 1: success */
	return 1;
}

/* Load a tag list from config file */
tagListType *load_taglist(char *filename)
{
	FILE *fp;
	char buf[BUF_SIZE];

	tagListType *list = create_taglist();
	list->filename = strdup(filename);

	fp = fopen(list->filename, "r");
	if (fp) {
		/* Make sure no garbage in memory */
		empty_taglist(list);
		while(!feof(fp)) {
			buf[0] = 0;
			tagItemType *item;

			/* Get line by line */
			fgets(buf, sizeof(buf), fp);
			if (!buf[0])
				break;

			/* Get tag and value */
			item = parse_tagitem(buf);
			if (item) {
				insert_taglist(list, item);
			}
		}
	}

	return list;
}

/* Get value from the taglist */
char *get_taglist(tagListType *list, char *tag)
{
	int pos;

	return (pos = search_taglist(list, tag)) >= 0 ?
		list->item[pos]->value : NULL;
}

/*
 * Update taglist
 * 1. Tag exist, simple update it
 * 2. No tag, create a new tag
 */
static int update_taglist(tagListType *list, char *tag, char *value)
{
	int pos;
	char *pch = NULL;
	tagItemType *newItem;

	if (!list || !tag || !value)
		return 0;

	/* Make sure no \r and \n in tag and value */
	if ((pch = strstr(tag, "\n")) != NULL)
		*pch = '\0';
	if ((pch = strstr(value, "\n")) != NULL)
		*pch = '\0';

	if ((pos = search_taglist(list, tag)) >= 0) {
		/* 1. Tag exists and simple update value */
		list->item[pos]->value = strdup(value);
	} else {
		/* 2. Create a new item */
		newItem = create_tagitem(tag, value);
		if (newItem)
			insert_taglist(list, newItem);
		else
			return 0;
	}

	/* 1: success */
	return 1;
}

/* Free the memory of the tag list */
void free_taglist(tagListType *list)
{
	if (list) {
		empty_taglist(list);

		if (list->memsize > 0) {
			list->memsize = 0;
			if (list->item) {
				free(list->item);
				list->item = NULL;
			}
		}
		if (list->filename) {
			free(list->filename);
			list->filename = NULL;
		}
	}
	return;
}

int update_file(const char *path, tagListType *list, char *tag, char *value)
{
	int i;
	FILE *fp;
	char pathTmp[64];

	if (!path || !list || !tag || !value)
		return 0;

	/* Update taglist first */
	if (!update_taglist(list, tag, value))
		return 0;

	snprintf(pathTmp, sizeof(pathTmp), "%s_tmp", path);
	fp = fopen(pathTmp, "w+");
	if (!fp)
		return 0;

	for (i = 0; i < list->size; i++)
		fprintf(fp, "%s=\"%s\"\n", list->item[i]->tag, list->item[i]->value);

	fclose(fp);
	rename(pathTmp, path);

	/* 1: success */
	return 1;
}

#ifdef UNITTEST_STRUTILS_TAGLIST
/*
 * main used for testing only
 * argv[1] config path
 * argv[2] tag
 * argv[3] value
 */
int main(int argc, char *argv[])
{
	char path[64];
	char *tag;
	char *value;
	char *tagValue;
	tagListType *list;

	if (argc < 3) {
		printf("Usage: config tag value\n");
		return 0;
	}


	/* Prepare variables */
	snprintf(path, sizeof(path), "%s", argv[1]);
	tag = strdup(argv[2]);
	value = strdup(argv[3]);

	printf("path: %s, tag: %s, new value: %s\n", path, tag, value);

	/* Let's play with it */
	list = load_taglist(path);
	if (list) {
		tagValue = get_taglist(list, tag);
		if (tagValue)
			printf("Tag=\"%s\"\n", tagValue);
		else
			printf("No Tag exists\n");

		/*  Update it now */
		if (update_file(path, list, tag, value))
			printf("Update taglist success\n");
		else
			printf("Failed to update taglist\n");
	} else {
		printf("Failed to load tag list\n");
	}
	free_taglist(list);

	return 0;
}

#endif /* UNITTEST_STRUTILS_TAGLIST */
