#ifndef __STRUTILS_H
#define __STRUTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_SIZE	1024

typedef struct
{
	char *tag;
	char *value;
} tagItemType;

typedef struct
{
	int size;
	int memsize;
	tagItemType **item;
	char *filename;
} tagListType;

/* Load a tag list from config file */
tagListType *load_taglist(char *filename);

/* Get value from the taglist */
char *get_taglist(tagListType *list, char *tag);

/* Update tag items in taglist */
int update_file(const char *path, tagListType *list, char *tag, char *value);

/* Free the memory of the tag list */
void free_taglist(tagListType *list);

#endif /* __STRUTILS_H */
